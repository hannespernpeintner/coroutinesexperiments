import kotlinx.coroutines.experimental.*
import kotlinx.coroutines.experimental.channels.Channel

fun main(args: Array<String>) {

    val workItems = Channel<String>(3)

    println("Starting generation")
    val producerPool = newFixedThreadPoolContext(1, "")
    Thread {
        for (i in 0 .. 20) {
            launch(producerPool) {
                println("Launching workItem $i")
                workItems.send("Item $i")
                println("Launched workItem $i")
            }
            Thread.sleep(300)
        }
        workItems.close()
    }.start()

    println("Starting workers")
    runBlocking(newFixedThreadPoolContext(8, "")) {
        println("Worker started")
        for (item in workItems) {
            launch(coroutineContext) {
                println("Thread ${Thread.currentThread().id} processes $item")
                Thread.sleep(300)
            }
        }
        println("Worker ended")
    }

    println("Main ended")
}